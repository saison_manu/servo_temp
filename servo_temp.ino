// Include the libraries we need
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Servo.h>

//Config T° sensor
// Data wire is plugged into port 7 on the Arduino
#define ONE_WIRE_BUS 7
// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

//config light captor
int lightPin = A0; //light sensor pin variable
int light = 0; //light value
int lowLight = 666; //light threshold value

//config servo
Servo sg90;//create servo object
int pos = 180; //init position variable
int oldPos = 0;//init old position variable
float t = 0; //init T° variable for servo

//config LED
int pinLed = 10;//Low temp led
int pinLed2 = 11;//High temp led
int pinLed3 = 12;//light led


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);//Init RS232
  pinMode(lightPin, INPUT); //Init light sensor
  sensors.begin();//start sensor library
  sg90.attach(4);//Init servo pin
  sg90.write(pos);//Init servo position 0
  Serial.print(F("Init servo to 0..."));
  pinMode(pinLed, OUTPUT);//init leds pin
  pinMode(pinLed2, OUTPUT);
  pinMode(pinLed3, OUTPUT);
  delay(500);//pause like doing something :-)
}

void loop() {
  // put your main code here, to run repeatedly:
  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");
  // After we got the temperatures, we can print them here.
  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
  Serial.print("Temperature for the device 1 (index 0) is: ");
  t = sensors.getTempCByIndex(0); //put Celsius T° in variable
  Serial.println(t);//print T°
  
  //led lighting with T°
  if (t < 25)
  {
    digitalWrite(pinLed, 1);//blue led for cold
  }
  else
  {
    digitalWrite(pinLed, 0);
  }

  if (t > 25)
  {
    digitalWrite(pinLed2, 1);//red led form warm
  }
  else
  {
    digitalWrite(pinLed2, 0);
  }
  
  //move servo with T°
    //convert T° to degres for servo
  t = constrain(t, 10, 40); //range for T°
  //  Higher angular degrees fed to the servo are counter-clockwise. IOW, setting 180 is all the way left on a dial.
  //  This logic reverses the direction and allocates 4.5 degrees of rotation per degree fahrenheit. That's how I designed the dial
  pos = (40 - t) * 6.5; //Celsius move : 6.5° for 1°C

  // Don't bother with the servo unless there's something to do
  if (pos != oldPos)
  {
    sg90.write(pos);//move servo to T°
    oldPos = pos;//put pos in oldPos
  }
  Serial.print("Position : ");
  Serial.println(pos);//print position
  
  //light captor and led command
  Serial.print("Light captor value : ");
  light = analogRead(lightPin); //read light captor value
  Serial.println(light);//print light value
  if (light <= lowLight)
  {
    digitalWrite(pinLed3, 1); //light on
  }
  else
  {
    digitalWrite(pinLed3, 0); //light off
  }

  delay(500);//pause for sensor accisition
}
